
module sais;

import std.stdio;

class sais(T) {
  public:
	static void induceSort(ref T seq[], ref long sa[], int nchar) {
		// S/Lを保持するバッファと、カウンタを確保する。
		long[] ccnt; ccnt.length = nchar+1;
		ccnt[] = 0;
		char[] slbuf; slbuf.length = seq.length;
		sa[] = -1;
		for(long i = 0; i < seq.length; i++) {
			ccnt[cast(int)seq[i]]++;				// seq[i]は$:0, A:1, C:2, G:3, T:4でコードしている想定。Aを0に振ってccnt[seq[i]-1]++;にしてもいいかもしれない。(将来的に1byteに4塩基入れる想定で)
		}
		// S/Lチェック
		slbuf[$-1] = STYPE;
		for(long i = seq.length-2; i >= 0; i--) {
			if(seq[i] > seq[i+1]) {
				slbuf[i] = LTYPE;
			} else if(seq[i] < seq[i+1]) {
				slbuf[i] = STYPE;
			} else {
				slbuf[i] = slbuf[i+1];
			}
		}
		// sort LMS suffixes
		// とりあえず適当に放り込む
		long[] mark; mark.length = nchar+1;
		mark[0] = ccnt[0] = 1;
		for(int i = 1; i < nchar+1; i++) {
			mark[i] = mark[i-1] + ccnt[i];
		}
		for(long i = slbuf.length-1; i > 0; i--) {
			if(isLMS(slbuf, i)) {
				sa[--mark[cast(int)seq[i]]] = i;
			}
		}
		sortLtype(seq, sa, slbuf, ccnt);
		sortStype(seq, sa, slbuf, ccnt);
		sortPrefix(seq, sa, slbuf, ccnt);
		// sort L-type suffixes
		sortLtype(seq, sa, slbuf, ccnt);
		// sort S-type suffixes
		sortStype(seq, sa, slbuf, ccnt);
		return;
	}
	static long[] getSuffixArray(ref T[] str, int nchar) {
		long[] sa; sa.length = str.length;//+1;
		induceSort(str/*~cast(T)0*/, sa, nchar);
		return sa;
	}
  private:
	const char STYPE = 1;
	const char LTYPE = 2;
	static bool isLMS(ref char[] x, long y) {
		return(y > 0 && x[y] == STYPE && x[y-1] == LTYPE);
	}
	static void sortLtype(ref T[] seq, ref long[] sa, ref char[] sl, ref long[] ccnt) {
		long[] mark; mark.length = ccnt.length;
		mark[0] = 0;
		for(int i = 1; i < mark.length; i++) {
			mark[i] = mark[i-1] + ccnt[i-1];
		}
		// sort from index 0
		for(long i = 0; i < seq.length; i++) {
			if(sa[i] > 0 && sl[sa[i]-1] == LTYPE) {
				sa[mark[cast(int)seq[sa[i]-1]]++] = sa[i] - 1;
			}
		}
		return;
	}
	static void sortStype(ref T[] seq, ref long[] sa, ref char[] sl, ref long[] ccnt) {
		long[] mark; mark.length = ccnt.length;
		mark[0] = ccnt[0];		// equals to 1
		for(int i = 1; i < mark.length; i++) {
			mark[i] = mark[i-1] + ccnt[i];
		}
		// sort from index len-1
		for(long i = seq.length-1; i >= 0; i--) {
			if(sa[i] > 0 && sl[sa[i]-1] == STYPE) {
				sa[--mark[cast(int)seq[sa[i]-1]]] = sa[i] - 1;
			}
		}
		return;
	}
	static void sortPrefix(ref T[] seq, ref long[] sa, ref char[] slbuf, ref long[] ccnt) {
		long n1 = 0;
		for(long i = 0; i < seq.length; i++) {
			if(isLMS(slbuf, sa[i])) {
				sa[n1++] = sa[i];
			}
		}
		for(long i = n1; i < seq.length; i++) {
			sa[i] = -1;
		}
		int name = 0;
		long prev = -1;
		for(long i = 0; i < n1; i++) {
			long pos = sa[i];
			bool diff = false;
			for(int d = 0; d < seq.length; d++) {
				if(prev == -1 || seq[pos+d] != seq[prev+d] || slbuf[pos+d] != slbuf[prev+d]) {
					diff = true; break;
				} else {
					if(d > 0 && (isLMS(slbuf, pos+d) || isLMS(slbuf, prev+d))) { break; }
				}
			}
			if(diff) {
				name++; prev = pos;
			}
			pos = (pos % 2 == 0) ? pos/2 : (pos-1)/2;
			sa[n1 + pos] = name-1;
		}
		for(long i = seq.length-1, j = seq.length-1; i >= n1; i--) {
			if(sa[i] >= 0) sa[j--] = sa[i];
		}
		long[] s1 = sa[$-n1..$];
		if(name < n1) {
			auto lsais = new sais!(long);
			lsais.induceSort(s1, sa[0..n1], name-1);
		} else {
			for(long i = 0; i < n1; i++) {
				sa[s1[i]] = i;
			}
		}
		long[] mark; mark.length = ccnt.length;
		mark[0] = ccnt[0];
		for(int i = 1; i < ccnt.length; i++) {
			mark[i] = mark[i-1] + ccnt[i];
		}
		for(long i = 1, j = 0; i < seq.length; i++) {
			if(isLMS(slbuf, i)) { s1[j++] = i; }
		}
		for(long i = 0; i < n1; i++) { sa[i] = s1[sa[i]]; }
		for(long i = n1; i < seq.length; i++) { sa[i] = -1; }
		for(long i = n1-1; i >=0; i--) {
			long j = sa[i];
			sa[i] = -1;
			sa[--mark[cast(int)seq[j]]] = j;
		}
		return;
	}
}

unittest {
	auto sa = new sais!(char);
	char[] str = cast(char[])"ATAATACGATAATAA" ~ cast(char)0;
	auto arr = sa.getSuffixArray(str, 128);
	assert(arr == [15, 14, 13, 10, 2, 5, 11, 8, 0, 3, 6, 7, 12, 9, 1, 4]);
	return;
}

