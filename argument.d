
module argument;

import std.stdio, std.regex;

class argument {
private:
	string[string] m_map;
public:
	this(string[] args) {
		for(int i = 0; i < args.length; i++) {
			if(args[i][0] == '-') {
				auto arg = replace(args[i], regex(r"-+"c), "");
				if(i == (args.length-1) || args[i+1][0] == '-') {
					m_map[arg] = "";
				} else {
					m_map[arg] = args[++i];
				}
			}
		}
		return;
	}
	~this() { return; }
	bool find(string opt) {
		return((opt in m_map) !is null);
	}
	string get(string opt) {
		if(find(opt)) {
			return(m_map[opt]);
		} else {
			return("");
		}
	}
	void set(string opt, string arg = "") {
		m_map[opt] = arg;
		return;
	}
	void add(string opt) {
		set(opt, "");
		return;
	}
}
