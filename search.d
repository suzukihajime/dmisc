
module search;

import sais;
version(unittest) {
	import std.stdio;
}

/*
Suffix Arrayを利用した検索
内部でBWTを作成し、それをBWT Searchに投げている。
*/
class sasearch(T, U...) {
private:
	long[] m_sa;
	bwtsearch!(T, U) m_bwtsch;
public:
	this(ref char[] str, int reduce = 1) {
		m_sa = sais.sais!(char).getSuffixArray(str, 128);
		writeln(m_sa);
		/*
		BWTを構築
		*/
		char[] bwt;
		bwt.length = str.length;
		for(long i = 0; i < bwt.length; i++) {
			if(m_sa[i] != 0) {
				bwt[i] = cast(char)str[m_sa[i] - 1];
			} else {
				bwt[i] = cast(char)0;
			}
		}
		writeln(bwt);
		m_bwtsch = new bwtsearch!(T, U)(bwt, reduce);
		return;
	}
	~this() { return; }
	long[] search(string query) {
		int lb, ub;
		long[] res;
		if(m_bwtsch.search(query, lb, ub)) {
			foreach(i; lb..ub+1) {
				res ~= m_sa[i];
			}
		}
		return(res);
	}
}

/*
BWTを利用した検索
中で補助表occを作成
*/
class bwtsearch(T, U...) {
private:
	char[] m_bwt;
	int m_reduce;
	static if(U.length > 0) {
		int[U[0].length+1] m_base;
		int[U[0].length+1][] m_occ;
	} else {
		int[T.max] m_base;
		int[T.max][] m_occ;
	}
	int[T.max] m_conv;
public:
	this(ref char[] bwt, int reduce = 1) {
		m_bwt = bwt;
		m_reduce = reduce;
		buildConvTable();
		buildOccTable();
		return;
	}
	~this() {
		return;
	}
	bool search(string query, ref int lb, ref int ub) {
		lb = lowerBound(cast(char[])query);
		ub = upperBound(cast(char[])query);
		writeln("search result of ", query, ": ", lb, ", ", ub);
		if(lb > ub) {
			lb = ub = -1;
			return(false);
		} else {
			return(true);
		}
	}
private:
	void buildConvTable() {
		m_conv[] = 0;
		int i = 1;
		foreach(ch; U[0]) {
			m_conv[cast(int)ch] = i++;
		}
		return;
	}
	void buildOccTable() {
		int[U[0].length+1] occtemp;
		m_occ.length = m_bwt.length / m_reduce + 1;
		occtemp[] = 0;
		for(int i = 0; i < m_bwt.length; i++) {
			++occtemp[m_conv[cast(int)m_bwt[i]]];
			if(i % m_reduce == 0) {
				m_occ[i/m_reduce] = occtemp;
			}
		}
		m_base[0] = 0;
		for(int i = 1; i < U[0].length+1; i++) {
			m_base[i] = m_base[i-1] + occtemp[i-1];
		}
		return;
	}
	int occ(char c, int i) {
		if(i < 0) { 
			return(0);
		} else {
			int[U[0].length+1] occtemp;
			occtemp = m_occ[i / m_reduce];
			while(i % m_reduce != 0) {
				++occtemp[m_conv[cast(int)m_bwt[i--]]];
			}
			return(occtemp[m_conv[cast(int)c]]);
		}
	}
	int lowerBound(char[] query) {
		if(query.length == 0) {
			return(0);
		} else {
			return(m_base[m_conv[query[0]]] + occ(query[0], lowerBound(query[1..$])-1));
		}
	}
	int upperBound(char[] query) {
		if(query.length == 0) {
			return(cast(int)m_bwt.length-1);
		} else {
			return(m_base[m_conv[query[0]]] + occ(query[0], upperBound(query[1..$])) - 1);
		}
	}
}

unittest {
}