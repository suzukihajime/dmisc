
module fasta;

import std.stdio;
import std.array;
import std.regex;
import std.algorithm : min, swap;

class fastastream {
private:
	string m_filename;
	File *m_file;
	string m_seqname;		// 最後に読み込んだ配列の名前
	char[] m_seq;			// appenderに渡す配列
	Appender!(char[], char) *m_seqapp;	// 最後に読み込んだ配列のappender
	char[] m_oldlin;		// 次の読み込みまで保持しておくため
	char delegate(char) m_cconv;
public:
	@property string filename() { return m_filename; }
	
	this(string filename) {
		m_filename = filename;
		m_file = new File(m_filename, "r");
		m_seqapp = new Appender!(char[])(m_seq);
		m_cconv = delegate char(char c) { return c; };
		return;
	}
	this(string filename, char delegate(char) cconv) {
		this(filename);
		m_cconv = cconv;
		return;
	}
	~this() { }
	/*
	fastaを1配列ずつ読み込む関数
	読み込む配列がなくなったらnullを返す
	*/
	fasta readSeq() {
		if(m_file.eof()) {
			return(null);
		}
		extractSeqname();
		m_seqapp.clear();
		extractSequence();
		fasta fa = new fasta(m_seqname, m_seqapp.data.dup);
		return(fa);
	}
	/*
	残っている全配列を返す
	*/
	fasta[] readAll() {
		fasta fa;
		fasta[] faarr;
		while((fa = readSeq()) !is null) {
			faarr ~= fa;
		}
		return(faarr);
	}
	/*
	foreach用opApply opReverseApplyは実装しない(できない)
	*/
	int opApply(int delegate(ref fasta fa) dg) {
		int result = 0;
		fasta fa;
		while((fa = readSeq()) !is null) {
			if((result = dg(fa)) == 1) { break; }
		}
		return(result);
	}
private:
	void extractSeqname() {
		if(m_oldlin.length > 0 && m_oldlin[0] == '>') {
			setSeqname(m_oldlin);
		} else {
			foreach(lin; m_file.byLine()) {
				if(lin.length > 0 && lin[0] == '>') {
					setSeqname(lin);
					break;
				}
			}
		}
	}
	void setSeqname(char[] seqname) {
		m_seqname = replace(cast(string)seqname, regex(r">\s*"c), "");		// 先頭の>を除去する
		return;
	}
	void extractSequence() {
		foreach(lin; m_file.byLine()) {
			if(lin.length > 0 && lin[0] != '>') {
				foreach(ref char c; lin) {
					c = m_cconv(c);
				}
				m_seqapp.put(lin[0 .. $]);
			} else {
				/* fastaが読み終わった */
				m_oldlin = lin;
				break;
			}
		}
		return;
	}
}

class fasta {
private:
	string m_seqname;
	char[] m_seq;
public:
	@property string seqname() { return m_seqname; }
	@property string seqname(string seqname) { return m_seqname = seqname; }
	@property ref char[] seq() { return m_seq; }
	@property ref char[] seq(ref char[] seq) {  m_seq = seq; return(m_seq); }
	@property ulong length() { return m_seq.length; }
	@property ulong length(ulong l) { return(m_seq.length = l); }
	
	this() {
		this("", "");
		return;
	}
	this(string seqname, char[] seq) {
		m_seqname = seqname; m_seq = seq;
		return;
	}
	this(string seqname, string seq) {
		this(seqname, cast(char[])seq);
		return;
	}
	~this() { return; }
	
	/* インデックス演算子: fa[i]; */
	char opIndex(size_t i) {
		if(m_seq.length > i) {
			return m_seq[i];
		} else {
			return 0;
		}
	}
	/* インデックス代入演算子: fa[i] = c; */
	char opIndexAssign(char val, size_t i) {
		if(m_seq.length <= i) {
			m_seq.length = i+1;
		}
		m_seq[i] = val;
		return(val);
	}
	/* スライス演算子: fa[]; / fa[i..j]; / fa[] = c; / fa[i..j] = c; */
	fasta opSlice() {
		return(this);
	}
	fasta opSlice(size_t x, size_t y) {
		if(x > y) { swap(x, y); }
		if(y >= m_seq.length || x >= m_seq.length) {
			assert(0, "Range violation in slice operator.");
		}
		fasta fa = new fasta(m_seqname, m_seq[x..y]);
		return(fa);
	}
	char opSliceAssign(char val) {
		return(opSliceAssign(val, 0, m_seq.length-1));
	}
	char opSliceAssign(char val, size_t x, size_t y) {
		if(x > y) { swap(x, y); }
		if(y >= m_seq.length || x >= m_seq.length) {
			assert(0, "Range violation in slice operator.");
		}
		for(size_t i = x; i <= y; i++) {
			m_seq[i] = val;
		}
		return(val);
	}
	/* toString */
	string text(int width = 70) {
		char[] str;
		auto strapp = Appender!(char[])(str);
		strapp.put(">");
		strapp.put(m_seqname);
		for(int i = 0; i < m_seq.length; i+=width) {
			strapp.put("\n");
			strapp.put(m_seq[i .. min(i+width, m_seq.length)]);
		}
		return(cast(string)strapp.data());
	}
	/* 結合演算子 */
	fasta opBinary(string op)(fasta b) {
		static if(op == "~") {
			fasta fa = new fasta(m_seqname~b.seqname(), m_seq~b.seq());
			return(fa);
		} else {
			assert(0, "Operator "~op~" is not implemented.");
			return(this);
		}
	}
	fasta opBinary(string op)(string b) {
		static if(op == '~') {
			fasta fa = new fasta(m_seqname, m_seq~b);
			return(fa);
		} else {
			assert(0, "Operator "~op~" is not implemented.");
			return(this);
		}
	}
	/* 結合代入演算子 */
	void opOpAssign(string op)(fasta b) {
		static if(op == "~") {
			m_seqname ~= b.seqname();
			m_seq ~= b.seq();
		} else {
			assert(0, "Operator "~op~" is not implemented.");
		}
		return;
	}
	void opOpAssign(string op)(string b) {
		static if(op == "~") {
			m_seq ~= b;
		} else {
			assert(0, "Operator "~op~" is not implemented.");
		}
	}
	/* compementな配列を取り出す */
	fasta complement() {
		fasta fa = new fasta(m_seqname ~ "_reverse_comp", m_seq.dup);
		for(int i = 0; i < m_seq.length; i++) {
			fa[i] = comp(m_seq[$-i-1]);
		}
		return(fa);
	}
private:
	char comp(char ch) {
		switch(ch) {
			case 'A': return 'T';
			case 'C': return 'G';
			case 'G': return 'C';
			case 'T': return 'A';
			default: return ch;
		}
	}
}

class fastqstream {
private:
	string m_filename;
	File *m_file;
	string m_seqname;		// 最後に読み込んだ配列の名前
	char[] m_seq;			// appenderに渡す配列
	Appender!(char[], char) *m_seqapp;	// 最後に読み込んだ配列のappender
	char[] m_qual;			// appenderに渡す配列
	Appender!(char[], char) *m_qualapp;	// 最後に読み込んだ配列のappender
	char[] m_oldlin;		// 次の読み込みまで保持しておくため
	char delegate(char) m_cconv;
public:
	@property string filename() { return m_filename; }
	
	this(string filename) {
		m_filename = filename;
		m_file = new File(m_filename, "r");
		m_seqapp = new Appender!(char[])(m_seq);
		m_qualapp = new Appender!(char[])(m_qual);
		m_cconv = delegate char(char c) { return c; };
		return;
	}
	this(string filename, char delegate(char) cconv) {
		this(filename);
		m_cconv = cconv;
		return;
	}
	~this() { }
	/*
	fastqを1配列ずつ読み込む関数
	読み込む配列がなくなったらnullを返す
	*/
	fastq readSeq() {
		if(m_file.eof()) {
			return(null);
		}
		extractSeqname();
		m_seqapp.clear();
		extractSequence();
		extractQualname();
		m_qualapp.clear();
		extractQuality();
		fastq fa = new fastq(m_seqname, m_seqapp.data.dup, m_qualapp.data.dup);
		return(fa);
	}
	/*
	残っている全配列を返す
	*/
	fastq[] readAll() {
		fastq fa;
		fastq[] faarr;
		while((fa = readSeq()) !is null) {
			faarr ~= fa;
		}
		return(faarr);
	}
	/*
	foreach用opApply opReverseApplyは実装しない(できない)
	*/
	int opApply(int delegate(ref fastq fa) dg) {
		int result = 0;
		fastq fa;
		while((fa = readSeq()) !is null) {
			if((result = dg(fa)) == 1) { break; }
		}
		return(result);
	}

private:
	void extractSeqname() {
		if(m_oldlin.length > 0 && m_oldlin[0] == '@') {
			setSeqname(m_oldlin);
		} else {
			foreach(lin; m_file.byLine()) {
				if(lin.length > 0 && lin[0] == '@') {
					setSeqname(lin);
					break;
				}
			}
		}
	}
	void extractQualname() {
		if(m_oldlin.length > 0 && m_oldlin[0] == '+') {
			setSeqname(m_oldlin);
		} else {
			foreach(lin; m_file.byLine()) {
				if(lin.length > 0 && lin[0] == '+') {
					setSeqname(lin);
					break;
				}
			}
		}
	}
	void setSeqname(char[] seqname) {
		m_seqname = replace(cast(string)seqname, regex(r"\+\s*"c), "");		// 先頭の>を除去する
		return;
	}
	void extractSequence() {
		foreach(lin; m_file.byLine()) {
			if(lin.length > 0 && lin[0] != '+') {
				foreach(ref char c; lin) {
					c = m_cconv(c);
				}
				m_seqapp.put(lin[0 .. $]);
			} else {
				/* fastqが読み終わった */
				m_oldlin = lin;
				break;
			}
		}
		return;
	}
	void extractQuality() {
		foreach(lin; m_file.byLine()) {
			if(lin.length > 0 && lin[0] != '@') {
				foreach(ref char c; lin) {
					c = m_cconv(c);
				}
				m_qualapp.put(lin[0 .. $]);
			} else {
				/* fastqが読み終わった */
				m_oldlin = lin;
				break;
			}
		}
		return;
	}
}

class fastq : fasta {
private:
	char[] m_qual;
public:
	@property ref char[] qual() { return m_seq; }
	@property ref char[] qual(ref char[] seq) {  m_seq = seq; return(m_seq); }
	
	this() {
		this("", "", "");
		return;
	}
	this(string seqname, char[] seq, char[] qual) {
		super(seqname, seq);
		m_qual = qual;
		return;
	}
	this(string seqname, string seq, string qual) {
		this(seqname, cast(char[])seq, cast(char[])qual);
		return;
	}
	~this() { return; }
	
	override {
		string text(int width = 70) {
			char[] str;
			auto strapp = Appender!(char[])(str);
			strapp.put("@");
			strapp.put(m_seqname);
			for(int i = 0; i < m_seq.length; i+=width) {
				strapp.put("\n");
				strapp.put(m_seq[i .. min(i+width, m_seq.length)]);
			}
			strapp.put("+");
			strapp.put(m_seqname);
			for(int i = 0; i < m_seq.length; i+=width) {
				strapp.put("\n");
				strapp.put(m_qual[i .. min(i+width, m_seq.length)]);
			}
			return(cast(string)strapp.data());
		}
	}
}

/*
unittest
fastatest.faが必要。
*/
unittest {
	// fastastreamのtest
	auto file = File("fastatest.fa", "w");
	file.writeln(">seq1\nAAAATTTT\n>seq2\nATATGCGC\n>seq4\natatatat\n>seq3\nGCGCGGGT");
	file.close();
	char toupper(char c) { return((c >= 'a' && c <= 'z') ? cast(char)(c + 'A' - 'a') : cast(char)c); }
	auto fas = new fastastream("fastatest.fa", &toupper);
	assert(fas.filename() == "fastatest.fa");
	assert(fas.readSeq().seq == "AAAATTTT");
	assert(fas.readSeq().seqname == "seq2");
	assert(fas.readSeq().seq == "ATATATAT");
	foreach(fasta fa; fas) {
		assert(fa.seq == "GCGCGGGT");
		assert(fa.seqname == "seq3");
	}
	auto fa = new fasta();
	assert(fa.seqname() == "");
	assert(fa.seqname("seqA") == "seqA");
	fa[0] = 'A';
	assert(fa[0] == 'A');
	fa[1] = 'T';
	assert(fa[1] == 'T');
}
