
progress.d -- Multi-threaded progress bar implementation in D programming language
==========

# Usage
Simple single-threaded variant:

	auto prog = new progress();
	prog.begin(5000);
	for(int i = 0; i < 5000; i++) {
		for(int k = 0; k < 1000; k++) {}
		prog.setval(i);
	}
	prog.end();

