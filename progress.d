
module progress;

import std.stdio, std.datetime, std.math, std.conv, core.thread;

class multiprogress {
private:
	int m_linecnt;
	struct pos {
		int x;
		int y;
		pos opBinary(string op)(pos b) {
			pos c;
			static if(op == "+") {
				c.x = x + b.x; c.y = y + b.y;
			} else static if(op == "-") {
				c.x = x - b.x; c.y = y - b.y;
			}
			return(c);
		}
	};
	struct proginfo {
		long m_stime;
		int m_max;
		int m_currRate, m_lastRate;
		pos m_cur;
	};
	proginfo[int] m_piarr;
	const double BAR_LENGTH = 50.0;
	const double TICK_PER_SECOND = 10_000_000.0;
public:
	this() {
		m_linecnt = 0;
		stdout.flush();
		return;
	}
	~this() {
		stdout.flush();
	}
	mp_inst newline() {
		synchronized(this) {
			stdout.write('\n'); stdout.flush();
			m_piarr[m_linecnt] = proginfo();
			m_piarr[m_linecnt].m_cur.y = m_linecnt;
			return(mp_inst(m_linecnt++, &begin, &setval, &end, &del, &message));
		}
	}
private:
	void begin(int id, string msg, int max) {
		m_piarr[id].m_stime = Clock.currStdTime();
		m_piarr[id].m_max = max;
		m_piarr[id].m_lastRate = m_piarr[id].m_currRate = 0;
//		m_piarr[id].m_cur.y = id;
		write(id, msg);
		return;
	}
	void setval(int id, int val) {
		double step = m_piarr[id].m_max / BAR_LENGTH;
		m_piarr[id].m_currRate = cast(int)floor(val / step);
		if(m_piarr[id].m_currRate > m_piarr[id].m_lastRate) {
			foreach(unused; m_piarr[id].m_lastRate..m_piarr[id].m_currRate) { write(id, "."); }
			m_piarr[id].m_lastRate = m_piarr[id].m_currRate;
		}
		return;
	}
	void end(int id, string msg = "Done") {
		setval(id, m_piarr[id].m_max);
		long elapse = Clock.currStdTime() - m_piarr[id].m_stime;
		msg ~= "(";
		msg ~= to!string(elapse / TICK_PER_SECOND);
		msg ~= " sec)";
		write(id, msg);
		return;
	}
	void del(int id) {
		m_piarr.remove(id);
		return;
	}
	void write(int id, string msg) {
		synchronized(this) {
			move(m_piarr[id].m_cur - pos(0, m_linecnt));
			stdout.write(msg); stdout.flush();
			m_piarr[id].m_cur.x += msg.length;
			move(pos(0, m_linecnt) - m_piarr[id].m_cur);
		}
		return;
	}
	void move(pos diff) {
		if(diff.x > 0) { stdout.write("\x1b[", diff.x, "C"); }
		else if(diff.x < 0) { stdout.write("\x1b[", -diff.x, "D"); }
		if(diff.y > 0) { stdout.write("\x1b[", diff.y, "B"); }
		else if(diff.y < 0) { stdout.write("\x1b[", -diff.y, "A"); }
		stdout.flush();
		return;
	}
	void message(int id, string msg) {
		synchronized(this) {
			m_piarr[id].m_cur.x = 0;
			move(m_piarr[id].m_cur - pos(0, m_linecnt));
			stdout.write("\x1b[2K");
			stdout.write(msg); stdout.flush();
			m_piarr[id].m_cur.x += msg.length;
			move(pos(0, m_linecnt) - m_piarr[id].m_cur);
		}
	}
}

struct mp_inst {
private:
	int m_id;
	void delegate(int id, string msg, int max) m_begin;
	void delegate(int id, int val) m_setval;
	void delegate(int id, string msg) m_end;
	void delegate(int id) m_delete;
	void delegate(int id, string msg) m_message;
public:
	@property int line() { return m_id; }
	this(int id,
	  void delegate(int, string, int) i_begin, void delegate(int, int) i_setval,
	  void delegate(int, string) i_end, void delegate(int) i_delete,
	  void delegate(int, string) i_message) {
		m_id = id;
		m_begin = i_begin;
		m_setval = i_setval;
		m_end = i_end;
		m_delete = i_delete;
		m_message = i_message;
		return;
	}
	~this() {
		m_delete(m_id);
		return;
	}
	void begin(int max = 4) {
		begin("Start", max);
		return;
	}
	void begin(string msg, int max = 4) {
		m_begin(m_id, msg, max);
		return;
	}
	void setval(int val) {
		m_setval(m_id, val);
		return;
	}
	void end(string msg ="Done") {
		m_end(m_id, msg);
		return;
	}
	void write(string msg) {
		m_message(m_id, msg);
		return;
	}
}

class progress {
private:
	long m_stime;
	int m_max;
	int m_currRate, m_lastRate;
	const double BAR_LENGTH = 50.0;
	const double TICK_PER_SECOND = 10_000_000.0;
public:
	this() {}
	~this() {}
	void begin(int max = 4) {
		begin("Start", max);
		return;
	}
	void begin(string msg, int max = 4) {
		m_stime = Clock.currStdTime();
		m_max = max;
		m_lastRate = m_currRate = 0;
		write(msg); stdout.flush();
		return;
	}
	void setval(int val) {
		double step = m_max / BAR_LENGTH;
		m_currRate = cast(int)floor(val / step);
		if(m_currRate > m_lastRate) {
			foreach(unused; m_lastRate..m_currRate) { write("."); }
			stdout.flush();
			m_lastRate = m_currRate;
		}
		return;
	}
	void end(string msg = "Done") {
		setval(m_max);
		write(msg);
		long elapse = Clock.currStdTime() - m_stime;
		writeln("(", elapse / TICK_PER_SECOND, " sec)");
		return;
	}
}

/*
void main() {
	auto prog = new progress();
	prog.begin(5000);
	for(int i = 0; i < 5000; i++) {
		for(int k = 0; k < 1000; k++) {}
		prog.setval(i);
	}
	prog.end();
	
	auto mprog = new multiprogress();
	class progthread : Thread {
	public:
		this(int wait) {
			m_wait = wait;
			super(&run);
			return;
		}
		~this() {}
	private:
		int m_wait;
		void run() {
			auto p = mprog.newline();
			p.begin(100);
			for(int i = 0; i < 100; i++) {
//				sleep(dur!("msecs")(m_wait));
				for(int k = 0; k < 2000000; k++) {}
				p.setval(i);
			}
			p.end();
			p.write("Execution done");
			return;
		}
	}
	progthread[] th;
	th.length = 100;
	for(int i = 0; i < 50; i++) {
		th[i] = new progthread(i * 30);
		th[i].start();
	}
	return;
}
*/
