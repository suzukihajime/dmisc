
module slicer;

import std.stdio, std.algorithm;

class slicer(T : T[]) {
private:
	T[] m_arr;
	int m_step, m_len;
public:
	this(T[] arr, int len, int step = int.init) {
		m_arr = arr;
		m_len = len;
		if(step == int.init) {
			m_step = len;
		} else {
			m_step = step;
		}
	}
	~this() { }
	int opApply(int delegate(ref T[] parr, ref int spos) dg) {
		int res = 0;
		for(int i = 0; i < m_arr.length; i+=m_step) {
			if((res = dg(m_arr[i..min(i+m_len, m_arr.length)], i)) == 1) { break; }
		}
		return(res);
	}
	int opApply(int delegate(ref T[] parr) dg) {
		int res = 0;
		for(int i = 0; i < m_arr.length; i+=m_step) {
			if((res = dg(m_arr[i..min(i+m_len, m_arr.length)])) == 1) { break; }
		}
		return(res);
	}
}
/*
void main() {
	string s = "Hello World!!";
	auto sl = new slicer!(char[])(cast(char[])s, 5);
	foreach(ps; sl) {
		writeln(ps);
	}
	return;
}
*/